const request = require('request-promise');

module.exports = {
    get: function (url) {
        return request(url).then(data => JSON.parse(data));
    }
}