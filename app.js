import { patch } from 'webdriver-js-extender';

const express = require('express');
const http = require('http');
const cors = require('cors');
const path = require('path');
const compression = require('compression');
const fs = require('fs');
const request = require('./utils/request');

const app = express();

app.use(cors({ origin: '*' }));
app.use(compression());
app.use(controlDomain, express.static(patch.join(__dirname, 'dist')));
app.get('*', controlDomain, (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});
const port = process.env.PORT || '4200';
app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => console.log(`Running localhost:${port}`));

function controlDomain(req, res, next) {
    let testDomain = "www.test.com";

    // request.get(`http://localhost:3000/api/apps/web/owner?domain=${testDomain}`)
    request.get(`https://reconnect-dev.herokuapp.com/api/apps/web/owner?domain=${req.headers.host}`)
        .then(data => {
            _writeTplData(data.result);
            next();
        });
}

function _writeTplData(ownerData) {
    let indexPath = path.join(__dirname, 'dist/index.html');
    let file = fs.readFileSync(indexPath, 'utf8');
    let code = `var __wt_o= ${JSON.stringify(ownerData)};`;
    file = file.replace(/<\/head>/g, '<script id="back-code">' + code + '</script></head>');

    fs.writeFileSync(indexPath, file, { encoding: 'utf8' });
}