declare global {

    interface String {
        contains(value: string, matchCase?: boolean): boolean;
        capitalize(): string;
        capitalizeAll(): string;
        limitWords(limit: number): string;
        formatEmail(): string;
        /**
         * Convert true o false in String to Boolean
         * and evalute if it's undefined.
         * Example: Parse 'true' => true, 'hello' => true, '' => false
         * @return boolean
         */
        toBoolean(): boolean;
        /**
         * Compare to a string value an returns true if they both are the same (Case sensitive by default)
         * Example 1: 'hello'.equals('Hello') returns false
         * Example 2: 'hello'.equals('Hello', false) returns true (case sensitive deactivated)
         * @return boolean
         */
        equals(value: string, matchCase?: boolean): boolean;
        toCamelCase(firstUpperCase: boolean): string;
    }

}

export class StringAugmentation {

    static init() {

        String.prototype.contains = function (value: string, matchCase: boolean = false): boolean {
            if (matchCase) return this.indexOf(value) > -1;
            return this.toLowerCase().indexOf(value.toLowerCase()) > -1;
        };

        String.prototype.capitalize = function (): string {
            return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();
        };

        String.prototype.capitalizeAll = function (): string {
            let getFirstCharsRegex = /(^|[^a-zA-Z\u00C0-\u017F'])([a-zA-Z\u00C0-\u017F])/g;
            return this.toLowerCase().replace(getFirstCharsRegex, char => char.toUpperCase());
        };

        String.prototype.limitWords = function (limit: number): string {
            let words = this.replace(/\s+/g, ' ').split(' ');

            if (words.length > limit) {
                let result = [];
                (limit).times(i => result.push(words[i]));
                return result.join(' ') + '...';
            }

            return this;
        };

        String.prototype.toBoolean = function (): boolean {
            let text = this.toLowerCase();
            return text === 'true' ? true : text === 'false' ? false : !!this;
        };

        String.prototype.equals = function (value: string, matchCase: boolean = false): boolean {
            if (matchCase) return this.toString() === value;
            else return this.toLowerCase() === value.toLowerCase();
        };

        String.prototype.formatEmail = function (): string {
            return this.trim().toLowerCase();
        };

        String.prototype.toCamelCase = function (firstUpperCase: boolean = true): string {
            let text = this.trim()
                .replace(/^\d+|\W|^_/g, ' ')    // begins with numbers, symbols or underscore
                .capitalizeAll()
                .replace(/\s/g, '');            // all blanks

            if (firstUpperCase) return text;

            return text.charAt(0).toLowerCase() + text.substr(1);
        };

    }

}