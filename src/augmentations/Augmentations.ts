import { ArrayAugmentation } from './ArrayAugmentation';
import { DateAugmentation } from './DateAugmentation';
import { NumberAugmentation } from './NumberAugmentation';
import { ObjectAugmentation } from './ObjectAugmentation';
import { StringAugmentation } from './StringAugmentation';

// *******************************************
// TODO: include augmentations directly to TS scope

// Number.prototype.times = times;

// export function times(callback) {
//     for (var i = 0; i < this; i++) {
//         callback(i);
//     }
// };
// *******************************************

export class Augmentations {

    static init() {
        ArrayAugmentation.init();
        DateAugmentation.init();
        NumberAugmentation.init();
        ObjectAugmentation.init();
        StringAugmentation.init();
    }

}