declare global {

    interface Date {
        getCurrentFullDate(): string;
    }

}

export class DateAugmentation {

    static init() {

        Date.prototype.getCurrentFullDate = function (): string {
            return `${this.getDate()}/${this.getMonth() + 1}/${this.getFullYear()}`;
        };

    }

}