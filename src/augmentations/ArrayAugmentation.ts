declare global {

    interface Array<T> {
        contains(value: any): boolean;
        distinct(): Array<T>;
        findByIndexes(indexes: number[]): Array<T[]>;
        randomize(count: number): Array<T[]>;
    }

}

export class ArrayAugmentation {

    static init() {

        Array.prototype.contains = function (value: any) {
            if (typeof value !== 'object') return this.indexOf(value) > -1;

            return !!this.find(item => JSON.stringify(item) === JSON.stringify(value));
        };

        Array.prototype.distinct = function () {
            var result = [];

            for (var i = 0; i < this.length; i += 1) {
                if (!result.contains(this[i])) result.push(this[i]);
            }

            return result;
        };

        Array.prototype.findByIndexes = function (indexes: number[]) {
            return indexes.map(i => this[i]);
        }

        Array.prototype.randomize = function (count: number) {
            let indexes = this.length.randList();

            if (count > this.length) count = this.length;

            return this.findByIndexes(indexes).slice(0, count);
        }

    }

}