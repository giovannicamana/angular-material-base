declare global {

    interface Number {
        times(callback: any, skip?: number): void;
        itemsOf(item: any, ext?: any, id?: string | boolean): any[];
        randList(distinct?: boolean): number[];
    }

}

export class NumberAugmentation {

    static init() {

        Number.prototype.times = function (callback, skip: number = 1): void {
            for (var i = 0; i < this; i += skip) {
                callback(i);
            }
        };

        // TEST:
        // console.log((5).itemsOf(5));
        // console.log((5).itemsOf(5, {}, true));
        // console.log((5).itemsOf({ name: 'Boris' }, {}, true));
        // console.log((5).itemsOf({ name: 'Boris' }, {}, 'USER'));
        // console.log((5).itemsOf({ name: 'Boris' }, { role: 'developer' }, 'PE'));
        Number.prototype.itemsOf = function (item: any, ext: any, id?: string | boolean): any[] {
            let list = [];

            (this).times(i => {
                let isObject = typeof (item) === 'object';

                if (!isObject && !ext) {
                    list.push(item);
                } else {
                    if (!isObject && ext) item = { value: item };

                    if (id) {
                        if (typeof (id) === 'string') item['id'] = id + (i + 1);
                        if (typeof (id) === 'boolean') item['id'] = (i + 1);
                    }

                    list.push(Object.assign({}, item, ext));
                }
            });

            return list;
        }

        Number.prototype.randList = function (distinct: boolean = true): number[] {
            let newList = [];

            do {
                let rand = Math.round(Math.random() * (this - 1));

                if (distinct) {
                    if (!newList.contains(rand)) newList.push(rand);
                } else {
                    newList.push(rand);
                }

            } while (newList.length < this);

            return newList;
        }
    }

}