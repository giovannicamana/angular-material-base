import { Component, ViewChild, Renderer, ElementRef, HostListener, ChangeDetectorRef, OnInit } from '@angular/core';

import { GeolocationService } from './shared/providers/utils/geolocation.service';
import { SharedConstants } from './shared/shared.constants';
import { Owner } from './shared/models/owner';

import { MediaMatcher } from '@angular/cdk/layout';
import { UserService } from './core/providers/user.service';
import { IdentityService } from './core/providers/session/identity.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [MediaMatcher]
})
export class AppComponent implements OnInit {

    title = 'Balaced Score Card';
    private _mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
    user: any;
    @ViewChild('modenav') modenav;
    @ViewChild('sidenav') sidenav;

    displayTitle: boolean = false;

    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _media: MediaMatcher,
        private _identitySrv: IdentityService,
        private _userSrv: UserService
    ) {
        this.mobileQuery = this._media.matchMedia('(max-width: 800px)');
        this._mobileQueryListener = () => this._changeDetectorRef.detectChanges();

        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    onToggleMode(event) {
        this.modenav.toggle();
    }
    ngOnInit() {
        this.getUser();
    }

    getUser() {
        let username = "gcamana"
        if (this._identitySrv.isLoggedIn()) {
            this._identitySrv.getCurrentUser()
                .then(user => {
                    this.user = user;
                })
        } else {
            /*this._userSrv.getUserByName(username)
                .subscribe(user => {
                    this.user = user;
                    this._identitySrv.user = user;
                });*/
        }
    }

    onToggleSide(event) {
        this.sidenav.toggle();
    }
}
