import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

const ROUTES: Routes = [
    // { path: '', redirectTo: '', pathMatch: 'full' },
    { path: 'bsc', loadChildren: 'app/balanced-scorecard/balanced-scorecard.module#BalancedScorecardModule' },
    { path: '**', redirectTo: '' },
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
