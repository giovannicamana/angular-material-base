import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

import { BaseService } from "../../core/base.service";
import { HeadersService } from "../../core/headers.service";
import 'rxjs/add/operator/map';

import { Owner } from "../../shared/models/owner";
import { CommonService } from "../../core/common.service";
import { IdentityService } from "../../core/providers/session/identity.service";

let END_POINT = "Service"

@Injectable()
export class NewService extends BaseService<Owner>{
    constructor(
        private _http: HttpClient,
        private _headersSrv: HeadersService,
        private _commonSrv: CommonService
    ) {
        super(END_POINT, _http, _headersSrv, _commonSrv);
    }

    getEdudle(criteria, _id?: string): Observable<any> {
        //let query = this._commonSrv.buildApiUrl(END_POINT + "/getCurrent_Edudle");
        let query = this._commonSrv.normalizeQuery(END_POINT + "/getCurrent_Edudle", criteria);
        return this._http.get(query)
            .map((res: any) => {
                //console.log(res);
                if (res.response) return res.response;
                else throw res.code;
            });
    }

    getAllEdudles(): Observable<any> {
        let query = this._commonSrv.buildApiUrl(END_POINT + "/getEdudles");
        return this._http.get(query)
            .map((res: any) => {
                //console.log(res);
                if (res.response) return res.response;
                else throw res.code;
            });
    }

    editarEdudle(params): Observable<any> {
        let query = this._commonSrv.normalizeQuery(END_POINT + "/editarEdudle", params);
        return this._http.get(query)
            .map((res: any) => {
                //console.log(res);
                if (res.response) return res.response;
                else throw res.code;
            });
    }
}