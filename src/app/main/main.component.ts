import { Component, OnInit } from '@angular/core';
import { DemoService } from './providers/demo.service';

import { Owner } from '../shared/models/owner';
import { UserService } from '../core/providers/user.service';
import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Builder } from 'protractor';
import { validateConfig } from '@angular/router/src/config';
import { IdentityService } from '../core/providers/session/identity.service';

@Component({
    selector: 'main-component',
    templateUrl: './main.component.html',
    styleUrls: ['main.component.scss']
})
export class MainComponent implements OnInit {
    displayTitle: boolean = false;
    title = "Main";
    user:any;
    newUserForm: FormGroup;
    usernameControl: FormControl;

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _identitySrv: IdentityService
    ) {

    }

    ngOnInit() {
        this.newUserForm = this._buildForm();
        this._identitySrv.getCurrentUser().then(() => {
            this._identitySrv.subscriber.subscribe(user => {
                this.user = user;
            });
        });
    }

    goModule(nameModule: string) {
        switch (nameModule) {
            case 'bsc':
                this._goBsc();
            default:
                break;
        }
    }
    register() {
        try {
            this._validate();
            this._saveUser();
        } catch (error) {
            //TODO: settign error controller
        }
    }

    private _saveUser() {
        let newUserData = this.newUserForm.value
        console.log(newUserData);
    }

    private _goBsc() {
        this._router.navigate(['bsc'], { relativeTo: this._activatedRoute });

    }

    private _buildForm() {
        this.usernameControl = new FormControl('', Validators.required);

        let form = this._formBuilder.group({
            firstName: ['', Validators.required],
            username: this.usernameControl,
            lastName: ['', Validators.required],
            email: ['', Validators.email],
            password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            language: ['', Validators.required]
        });

        return form;
    }

    private _validate() {
        let form = this.newUserForm;
        if (!form.valid) throw "is Required";
    }

}
