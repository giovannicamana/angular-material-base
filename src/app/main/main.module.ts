import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MainRoutingModule } from './main-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';

import { SharedModule } from '../shared/shared.module';
import { MainComponent } from './main.component';
import { DemoService } from './providers/demo.service';
import { UserService } from '../core/providers/user.service';
import { NewService } from './providers/new.service';

import { DemoComponent } from './components/demo-compoenent/demo-compoenent.component';
import { NewComponent } from './components/new-comp/new-comp.component';
import { EditModalComponent } from './components/edit-modal/edit-modal.component';
import { SnackBarComponent } from './components/snackbar/snackbar.component';
import { TablaComponent } from './components/tabla.component/tabla.component';
import { ExcelService } from './providers/excel.service';

@NgModule({
    imports: [
        // CommonModule,
        MainRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        // material
        MatCardModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTableModule
    ],
    declarations: [
        MainComponent,
        DemoComponent,
        NewComponent,
        EditModalComponent,
        SnackBarComponent,
        TablaComponent
    ],
    entryComponents : [
        EditModalComponent,
        SnackBarComponent
    ],
    providers: [
        DemoService,
        UserService,
        NewService,
        ExcelService
    ]
})
export class MainModule { }