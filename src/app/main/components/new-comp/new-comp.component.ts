import { Component, OnInit, Input } from '@angular/core';
import { NewService } from '../../providers/new.service';
import { SharedConstants } from '../../../shared/shared.constants';
import { EditModalComponent } from '../edit-modal/edit-modal.component';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { SnackBarComponent } from '../snackbar/snackbar.component';

@Component({
    selector: 'new-component',
    templateUrl: 'new.component.html'
})
export class NewComponent implements OnInit {
    @Input() headerTitle: string;
    @Input('nombre') nombreTmp:string;

    UNKNOWN_USER_IMAGE : string = SharedConstants.PATHS.UNKNOWN_USER_IMAGE;
    valor  : string = 'Diego';
    myCss: any = {'color':'green'}

    edudle : any;
    my_edudles : Element[];

    constructor(
        private _newServ : NewService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar
    ) {
        this.edudle = this.edudle || [];
        this.my_edudles = this.my_edudles || [];
    }

    ngOnInit() {
        this.getEdudle();
        this.getAllEdudles();
    }

    getEdudle() {
        let criteria = {
            id:'3',
            sede:'323'
        }
        this._newServ.getEdudle(criteria)
            .subscribe(val => {
                this.edudle = val;
            });
    }

    getAllEdudles() {
        this._newServ.getAllEdudles()
            .subscribe(val => {
                console.log(val.edudles);
                console.log('new cmponent');
                this.my_edudles = val.edudles;//new MatTableDataSource(val.edudles);
            });
    }

    openModalEditEDoodle(edudleObj): void {
        let dialogRef = this.dialog.open(EditModalComponent, {
          width: '300px',
          data: { objEdudle : edudleObj }
        });
        dialogRef.afterClosed().subscribe(result => {
            if(result.tipo == 'EDIT') {
                let params = {
                    id_edudle  : result.obj.id_edudle,
                    titudle    : result.obj.titudle,
                    desc_dudle : result.obj.desc_dudle
                };
                this._newServ.editarEdudle(params)
                    .subscribe(val => {
                        this.showMsj(val.msj);
                    });
            }
        });
    }

    showMsj(msj : string): void {
        this.snackBar.openFromComponent(SnackBarComponent, {
            duration: 1000,
            data    : msj
          });
    }
}

export interface Element {
    titudle: string;
    desc_dudle: string;
    linkudle: string;
    imagedudle: string;
    flg_default: string;
    flg_acti: string;
}