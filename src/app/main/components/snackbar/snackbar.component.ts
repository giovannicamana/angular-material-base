import {Component, Inject} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {MAT_SNACK_BAR_DATA} from '@angular/material';

@Component({
    selector: 'snack-bar',
    templateUrl: 'snackbar.component.html',
    styles: [`.snackbarColor { color: white; }`],
})
export class SnackBarComponent {

    mensaje : string;

    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data: any
    ) {
        this.mensaje = data;
    }
}