import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoCompoenentComponent } from './demo-compoenent.component';

describe('DemoCompoenentComponent', () => {
  let component: DemoCompoenentComponent;
  let fixture: ComponentFixture<DemoCompoenentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoCompoenentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoCompoenentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
