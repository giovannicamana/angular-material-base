import { Component, OnInit, OnChanges } from '@angular/core';
import { UserService } from '../../../core/providers/user.service';
import { ExcelService } from '../../providers/excel.service';

@Component({
    selector: 'app-demo-compoenent',
    templateUrl: './demo-compoenent.component.html',
    styleUrls: ['./demo-compoenent.component.scss'],
})
export class DemoComponent implements OnInit {

    name: string = "Dark";
    color: string = "pink";
    isShow: boolean;
    lastName: string = "";

    constructor(
        private _excelSrv: ExcelService,
        private _userSrv: UserService
    ) { }

    //hook angular
    ngOnInit() {
        this.getUser();
    }

    //metodoa publico
    showMessage(): void {
        const igv: number = 0.018;
        let name: string = "Giovanni";
        this.isShow = !this.isShow;
        this.name = "Jimmmmmy";
    }

    getUser() {
        this._userSrv.getUserByName('gcamana')
            .subscribe(user => {
                console.log(user);
                this.exportAsExcelFile([user], "prueba");
            });
    }

    exportAsExcelFile(user: any[], nameFile: string) {
        this._excelSrv.exportAsExcelFile(user, nameFile)
    }
}
