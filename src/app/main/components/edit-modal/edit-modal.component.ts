import { Component, OnInit, Input, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'edit-modal',
    templateUrl: 'edit-modal.component.html',
})
export class EditModalComponent {
    
    datos : any;
    constructor(
      public dialogRef: MatDialogRef<EditModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.datos = data;
    }
  
    onNoClick(): void {
        let rpta = { tipo : 'CLOSE'};
        this.dialogRef.close(rpta);
    }

    editarEdudle(): void {
        let rpta = { tipo : 'EDIT', obj : this.datos.objEdudle};
        //console.log(this.datos.objEdudle.titudle);
        //llamar a mi service
        this.dialogRef.close(rpta);
    }
}