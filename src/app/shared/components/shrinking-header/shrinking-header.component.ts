import { Component, ElementRef, Output, EventEmitter, Input, Renderer, Renderer2, NgZone } from '@angular/core';
import { ShrinkHeaderService } from '../../providers/utils/shrink-header.service';

@Component({
    selector: 'shrinking-header-component',
    templateUrl: './shrinking-header.component.html'
})

export class ShrinkingHeaderComponent {

    @Input('headerHeight') headerHeight: any;
    @Output() collapsed = new EventEmitter<boolean>();
    isCollapsed: boolean = false;

    constructor(
        private _element: ElementRef,
        private _renderer: Renderer,
        private _renderer2: Renderer2,
        private _shrinkHeaderSrv: ShrinkHeaderService,
        private _zone: NgZone
    ) { }

    ngAfterViewInit() {
        this._renderer.setElementStyle(this._element.nativeElement, 'height', this.headerHeight + 'px');

        let element = document.querySelector('mat-sidenav-content');

        this._renderer2.listen(element, 'scroll', (event) => {
            this.resizeHeader(event);
        });
    }

    resizeHeader(ev) {
        let adjustedHeight;

        adjustedHeight = this.headerHeight - ev.target.scrollTop;


        let offset = Math.round(this.headerHeight / 1.2);

        if (adjustedHeight <= 0) {

            adjustedHeight = 0;

            if (!this.isCollapsed) {
                this.isCollapsed = true;

            }
        }

        if (adjustedHeight <= offset) {
            this._zone.run(() => {
                this.collapsed.emit(true);
                this._shrinkHeaderSrv.suscriber.emit(true);
            });
        }

        if (adjustedHeight > 0) {

            if (this.isCollapsed) {
                this.isCollapsed = false;
            }

        }
        if (adjustedHeight > offset) {
            this._zone.run(() => {
                this.collapsed.emit(false);
                this._shrinkHeaderSrv.suscriber.emit(false);
            });
        }

        this._renderer.setElementStyle(this._element.nativeElement, 'height', adjustedHeight + 'px');
    }
}
