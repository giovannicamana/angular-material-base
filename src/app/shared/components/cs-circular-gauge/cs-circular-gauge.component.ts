// https://codepen.io/pen/
import { Component, ViewEncapsulation, ViewChild, Input } from '@angular/core';

@Component({
    selector: 'cs-circular-gauge-component',
    template: '<chart [options]="options"></chart>',
    styles: [`
    chart {
      display: block;
    }
  `]
})

export class CsCircularGaugeComponent {
    options: Object;

    constructor() {
        this.options = {
            chart: {
                type: 'gauge',
                margin: [0, 0, 0, 0],
                size: '100%',
                backgroundColor: 'transparent',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false,
                dataLabels: false
            },
            title: '',
            exporting: { enabled: false },
            pane: {
                startAngle: -90,
                endAngle: 90,
                background: [{
                    borderWidth: 0,
                    backgroundColor: 'transparent'
                }]
            },
            credits: {
                enabled: false
            },

            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 0,
                minorTickLength: 100,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 0,
                tickWidth: 0,
                tickPosition: 'inside',
                tickLength: 0,
                tickColor: '#666',
                tickPositions: [34, 65],
                plotBands: [{
                    from: 0,
                    to: 34,
                    color: '#55BF3B'
                }, {
                    from: 34,
                    to: 65,
                    color: '#DDDF0D'
                }, {
                    from: 65,
                    to: 100,
                    color: '#DF5353'
                }]
            },

            tooltip: {
                enabled: false
            },

            series: [{
                name: 'Logro',
                data: [80],
                tooltip: {
                    valueSuffix: '%',
                    enabled: false
                },
                dial: {
                    backgroundColor: '#959595',
                    baseLength: '1%',
                    baseWidth: 6,
                    borderColor: '#959595'
                }
            }]
        };
    }

}