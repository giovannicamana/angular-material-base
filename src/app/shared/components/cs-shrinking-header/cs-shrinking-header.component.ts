import { Component, ElementRef, Output, EventEmitter, Input, Renderer, Renderer2, NgZone } from '@angular/core';
import { ShrinkHeaderService } from '../../providers/utils/shrink-header.service';

@Component({
    selector: 'cs-shrinking-header-component',
    templateUrl: './cs-shrinking-header.component.html'
})

export class CsShrinkingHeaderComponent {

    @Input() headerHeight: number = 256;
    @Input() isToolbar: boolean = true;
    @Input() offset: number = 64;
    @Input() fontSize: number = 56;

    constructor(
        private _element: ElementRef,
        private _renderer: Renderer,
    ) { }

    ngAfterViewInit() {
        let element = document.querySelector('mat-sidenav-content');
        this._renderer.setElementStyle(this._element.nativeElement, 'height', this.headerHeight + 'px');

        this._renderer.listen(element, 'scroll', (event) => {
            this.onScrollTop(event);
        });
    }

    onScrollTop(ev) {
        let titleElement = document.querySelector('.font-size-56');
        let header = document.querySelector('header-component .mat-toolbar');
        let scrollHeightTmp = ev.currentTarget.scrollHeight - ev.currentTarget.clientHeight;

        if (ev.currentTarget.scrollTop > (this.headerHeight / 2)) {
            titleElement.classList.add('hideTitle');
        } else {
            titleElement.classList.remove('hideTitle');
        }

        if (ev.currentTarget.scrollTop !== scrollHeightTmp) {
            let titleElement = document.querySelector('.font-size-56');
            let fontSizeTmp = this.fontSize;
            let marginBottom = 0;

            if (ev.currentTarget.scrollTop <= this.headerHeight) {
                marginBottom = ev.currentTarget.scrollTop / 4;
                header.classList.remove('shadow');
            } else {
                marginBottom = this.offset;
                header.classList.add('shadow');
            }

            if (ev.currentTarget.clientHeight > 0) {
                fontSizeTmp = fontSizeTmp - (ev.currentTarget.scrollTop / 6);
            } else {
                fontSizeTmp = fontSizeTmp + (ev.currentTarget.scrollTop / 6);
            }

            // this._renderer.setElementStyle(titleElement, 'font-size', fontSizeTmp + 'px');
            if (this.isToolbar) {
                this._renderer.setElementStyle(this._element.nativeElement, 'margin-bottom', marginBottom + 'px');
            }
        }
    }
}
