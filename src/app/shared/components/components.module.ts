import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-highcharts';

import { PipesModule } from '../pipes/pipes.module';
import { MatIconModule, MatToolbarModule, MatButtonModule } from '@angular/material';

import { ShrinkingHeaderComponent } from './shrinking-header/shrinking-header.component';
import { CsCircularGaugeComponent } from './cs-circular-gauge/cs-circular-gauge.component';
import { CsShrinkingHeaderComponent } from './cs-shrinking-header/cs-shrinking-header.component';

// Import your library
declare var require : any;
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PipesModule,
        ChartModule.forRoot(
            require('highcharts'),
            require('highcharts/modules/exporting'),
            require('highcharts/highcharts-more'),
            require('highcharts/modules/solid-gauge')
        )
    ],
    declarations: [
        ShrinkingHeaderComponent,
        CsShrinkingHeaderComponent,
        CsCircularGaugeComponent
    ],
    exports: [
        ShrinkingHeaderComponent,
        CsShrinkingHeaderComponent,
        CsCircularGaugeComponent
    ],
    providers: [

    ]
})
export class ComponentsModule { }
