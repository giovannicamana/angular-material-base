export class Owner {
    name?: string;
    blog?: string;
    bio?: string;
    avatar_url?: string;
    login?: string;
    company?: any;
    url?: any;
}