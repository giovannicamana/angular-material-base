export class SharedConstants {
    static get STORAGE_KEYS() {
        return {
            OWNER: {
                TOKEN: 'owner-token',
                DATA: 'owner-data'
            }
        };
    }

    static get storage() {
		return {
			CURRENT_USER: 'current-user',
			CURRENT_USER_TOKEN: 'current-user-token',
		};
	}

    static get GAUGE() {
        return {
            PRINCIPAL_GAUGE: {
                RULES: [
                    {
                        rule: '%v <= 10',
                        backgroundColor: '#FF3504'
                    },
                    {
                        rule: '%v > 10 && %v < 50',
                        backgroundColor: '#FFC324'
                    },
                    {
                        rule: '%v >= 50 && %v < 100',
                        backgroundColor: '#43ACA1'
                    }
                ],
                LABELS: ['0', '', '20', '', '', '50', '', '', '', '', '100'],
                SIZE_RING: 15,
                _ID: 'principal-gauge',
            },
            FINALCIAL_GAUGE: {
                RULES: [
                    {
                        rule: '%v <= 0',
                        backgroundColor: '#FF3504'
                    },
                    {
                        rule: '%v > 0 && %v < 25',
                        backgroundColor: '#FFC324'
                    },
                    {
                        rule: '%v >= 25 && %v < 100',
                        backgroundColor: '#43ACA1'
                    }
                ],
                LABELS: ['0', '10', '', '30', '', '', '', '', '', '', '100'],
                SIZE_RING: 13,
                _ID: 'financial-gauge',
                MARGIN_TOP: 0,
            },
            CLIENT_GAUGE: {
                RULES: [
                    {
                        rule: '%v <= 35',
                        backgroundColor: '#FF3504'
                    },
                    {
                        rule: '%v > 35 && %v < 75',
                        backgroundColor: '#FFC324'
                    },
                    {
                        rule: '%v >= 75 && %v < 100',
                        backgroundColor: '#43ACA1'
                    }
                ],
                LABELS: ['0', '', '', '', '40', '', '', '', '80', '', '100'],
                SIZE_RING: 13,
                _ID: 'client-gauge',
                MARGIN_TOP: 0,
            },
            INTERNAL_PROCCESS_GAUGE: {
                RULES: [
                    {
                        rule: '%v <= 15',
                        backgroundColor: '#FF3504'
                    },
                    {
                        rule: '%v > 15 && %v < 55',
                        backgroundColor: '#FFC324'
                    },
                    {
                        rule: '%v >= 55 && %v < 100',
                        backgroundColor: '#43ACA1'
                    }
                ],
                LABELS: ['0', '', '20', '', '', '', '60', '', '', '', '100'],
                SIZE_RING: 13,
                _ID: 'internal-proccess-gauge',
                MARGIN_TOP: 0,
            }
        }
    }

    static get links() {
        return {
            GOOGLE_PLAY_URL: 'https://play.google.com/store?hl=en',
            ITUNES_URL: 'https://www.apple.com/itunes/'
        };
    }

    static map = { DEFAULT_MILES_RADIUS: 2, DEFAULT_ZOOM: 14 };

    static GOOGLEMAPS = {
        API_KEY: "AIzaSyD631Bb9E2LNPrUJuzxQb--thSNEItBGNU",
        DEFAULT_ZOOM: 14
    }
    static countiesByState = {
        MA: ['Barnstable', 'Berkshire', 'Bristol', 'Dukes', 'Essex', 'Franklin', 'Hampden', 'Hampshire', 'Middlesex', 'Nantucket', 'Norfolk', 'Plymouth', 'Suffolk', 'Worcester']
    };

    static get PATHS() {
        return {
            UNKNOWN_USER_IMAGE: 'assets/images/user/unknown-user.jpg',
            UNKNOWN_LOGO_IMAGE: "./assets/img/logo.png",
            DEFAULT_BG_IMAGE: 'assets/images/profile-cover.jpg',
            DEFAULT_LOGO: 'assets/images/circlelogo.png',
            DEFAULT_PROPERTIES: './assets/img/single-property-01.jpg',
            DEFAULT_HOME: './assets/img/listing-01.jpg',

            BSC: {
                FRAME_1: 'assets/images/bsc/frame-1.png',
            },
        };
    };

    static get ROLES() {
        return {
            PROMOTOR: 'promotor',
        };
    }

    static get messages() {
        return {
            error: {
                SELECT_USER: 'You must select a user type.',
                CONNECT_WITH_FACEBOOK: 'Connect with Facebook in a real device.',
                UPLOAD_FILE: 'Unable to upload the image. Try, again later.',
                RETRIEVING_DATA: 'Error retrieving data.',
                SAVING_DATA: 'Error Saving Data.',
                PHONE_NUMBER: 'No phone number provided.',
                PASSWORD_MATCH: 'Passwords must match.',
                USERNAME_EXISTS: 'Username already exists.',
                ENTER_REQUIRED_INFO: 'Please, enter all the required information.',
                ENTER_NMLS: 'You must enter a NMLS number.',
                ENTER_PHONE: 'You must enter a phone number.'
            },
            info: {
                LOGGING_IN: 'Logging in, please wait...',
                SEARCHING_AGENTS: 'Searching Agents...',
                SEARCHING_BUYERS: 'Searching Buyers...',
                RETRIEVING_DATA: 'Retrieving data...',
                DEFAULT_HOME_MESSAGE: 'Look at this house, it seems very interesting.',
                SAVING_DATA: 'Saving Data...'
            }
        };
    };
}