import { NgModule } from "@angular/core";
import { CsCurrencyPipe } from "./cs-currency.pipe";
import { CsCapitalizePipe } from "./cs-capitalize.pipe";

@NgModule({
    declarations: [
        CsCurrencyPipe,
        CsCapitalizePipe
    ],
    exports: [
        CsCurrencyPipe,
        CsCapitalizePipe
    ]
})
export class PipesModule { }