import { Injectable, Pipe } from '@angular/core';

@Pipe({
    name: 'csCapitalize'
})
@Injectable()
export class CsCapitalizePipe {

    transform(text: string, args: string = '') {
        if (!text || typeof (text) !== 'string') return '';
        if (args && args.equals('all')) return text.capitalizeAll();
        return text.capitalize();
    }

}