import { NgModule } from '@angular/core';

import { HighlightDirective } from './highlight.directive';
import { CsEmailToDirective } from './cs-email-to.directive';
import { CsCallToDirective } from './cs-call-to.directive';

@NgModule({
    declarations: [
        HighlightDirective,
        CsCallToDirective,
        CsEmailToDirective
    ],
    exports: [
        HighlightDirective,
        CsCallToDirective,
        CsEmailToDirective
    ]
})
export class DirectivesModule { }
