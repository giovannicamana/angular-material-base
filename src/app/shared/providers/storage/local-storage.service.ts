import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    set(key: string, data: any): void {
        localStorage.setItem(key, JSON.stringify(data));
    }

    get(key: string): Promise<any> {
        let JSONStr = localStorage.getItem(key);
        let isJSONValid = JSONStr && JSONStr != 'undefined';
        return Promise.resolve(isJSONValid ? JSON.parse(JSONStr) : null);
    }

    remove(key: string): void {
        localStorage.removeItem(key);
    }

    clear(): void {
        localStorage.clear();
    }

}
