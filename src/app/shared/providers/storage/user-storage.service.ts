import { Injectable } from '@angular/core';


import { LocalStorageService } from './local-storage.service';

import { SharedConstants } from '../../shared.constants';

@Injectable()
export class UserStorageService {

    constructor(private _storageSrv: LocalStorageService) { }

    setCurrentUser(data: Object): void {
        this._storageSrv.set(SharedConstants.storage.CURRENT_USER, JSON.stringify(data));
    }

    getCurrentUser(): Promise<any> {
        return this._storageSrv.get(SharedConstants.storage.CURRENT_USER)
            .then(data =>{ data ? JSON.parse(data) : undefined});
    }

    removeCurrentUser(): void {
        return this._storageSrv.remove(SharedConstants.storage.CURRENT_USER);
    }

    clearAll() {
        return this._storageSrv.clear();
    }

    setToken(token: string): void {
        this._storageSrv.set(SharedConstants.storage.CURRENT_USER_TOKEN, token);
    }

    getToken(): Promise<string> {
        return this._storageSrv.get(SharedConstants.storage.CURRENT_USER_TOKEN)
                    .then(token => token ? token : undefined);
    }

    removeToken(): void {
        return this._storageSrv.remove(SharedConstants.storage.CURRENT_USER_TOKEN);
    }

}
