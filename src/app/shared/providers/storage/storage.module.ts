import { ModuleWithProviders, NgModule } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { IdentityService } from '../../../core/providers/session/identity.service';
import { UserStorageService } from './user-storage.service';


@NgModule()

export class StorageModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: StorageModule,
            providers: [
                LocalStorageService,
                IdentityService,
                UserStorageService
            ]
        };
    }
}