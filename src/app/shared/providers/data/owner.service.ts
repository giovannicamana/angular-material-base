import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { BaseService } from "../../../core/base.service";
import { HeadersService } from "../../../core/headers.service";
import { CommonService } from "../../../core/common.service";
import { ConfigService } from "../../../core/config.service";

import { Observable } from "rxjs/Observable";

import 'rxjs/add/operator/map';
import { IdentityService } from "../../../core/providers/session/identity.service";

let END_POINT = 'api/apps';
@Injectable()

export class OwnerService extends BaseService<any>{
    constructor(
        private _http: HttpClient,
        private _headersSrv: HeadersService,
        private _commonSrv: CommonService,
        private _IdentitySrv: IdentityService
    ) {
        super(END_POINT, _http, _headersSrv, _commonSrv);
    }

    getByDomainName(): Observable<any> {
        return super.getOne({ domainName: this._IdentitySrv.user.domain }, 'web/owner');
    }
}
