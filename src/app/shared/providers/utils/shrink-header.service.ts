import { CommonService } from "../../../core/common.service";
import { HttpClient } from "@angular/common/http";
import { Injectable, EventEmitter } from "@angular/core";


@Injectable()
export class ShrinkHeaderService {
    suscriber: EventEmitter<any>;
    suscriberHeader: EventEmitter<any>;

    constructor() {
        this.suscriber = new EventEmitter<any>();
        this.suscriberHeader = new EventEmitter<any>();
    }

}