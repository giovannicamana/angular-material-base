import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { BalancedScorecardRoutingModule } from './balanced-scorecard-routing.module';

import { BscWelcomeComponent } from './pages/bsc-welcome/bsc-welcome.component';
import { BscDashboardComponent } from './pages/bsc-dashboard/bsc-dashboard.component';


import {
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressBarModule

} from '@angular/material';

import { CsAccordion } from './components/cs-accordion/cs-accordion.component';
import { CsAccordionGroupComponent } from './components/cs-accordion-group/cs-accordion-group.component';
import { CsUserAvatarComponent } from './components/cs-user-avatar/cs-user-avatar.component';
import { CsGoalComponent } from './components/cs-goal/cs-goal.component';
import { CsIndicatorsComponent } from './components/cs-indicators/cs-indicators.component';

@NgModule({
    imports: [
        CommonModule,
        BalancedScorecardRoutingModule,
        SharedModule,

        // material
        MatCardModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatSidenavModule,
        MatToolbarModule,
        MatExpansionModule,
        MatProgressBarModule
    ],
    declarations: [
        BscWelcomeComponent,
        BscDashboardComponent,

        //Components
        CsAccordion,
        CsAccordionGroupComponent,
        CsUserAvatarComponent,
        CsGoalComponent,
        CsIndicatorsComponent
    ],
    providers: [
        // DemoService,
        // UserService
    ]
})
export class BalancedScorecardModule { }