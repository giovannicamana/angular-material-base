import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { SharedConstants } from '../../../shared/shared.constants';
import { MediaMatcher } from '@angular/cdk/layout';
import { IdentityService } from '../../../core/providers/session/identity.service';

@Component({
    selector: 'bsc-welcome-component',
    templateUrl: './bsc-welcome.component.html',
    styleUrls: ['bsc-welcome.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [MediaMatcher]
})
export class BscWelcomeComponent implements OnInit {
    user: any;
    mobileQuery: MediaQueryList;
    UNKNOWN_USER_IMAGE: string = SharedConstants.PATHS.UNKNOWN_USER_IMAGE;
    DOODLE: string = SharedConstants.PATHS.BSC.FRAME_1;
    title: string = "Balance Scorecard";
    displayTitle: boolean = false;
    private _mobileQueryListener: () => void;

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _identitySrv: IdentityService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _media: MediaMatcher
    ) {
        this.mobileQuery = this._media.matchMedia('(max-width: 800px)');
        this._mobileQueryListener = () => this._changeDetectorRef.detectChanges();

        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnInit() {
        this._identitySrv.getCurrentUser().then(() => {
            this._identitySrv.subscriber.subscribe(user => {
                this.user = user;
            });
        });
    }

    goToDashboard() {
        this._router.navigate(['/bsc/dashboard'], { relativeTo: this._activatedRoute });
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }
}
