import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef, Renderer } from '@angular/core';
import { SharedConstants } from '../../../shared/shared.constants';
import { MediaMatcher } from '@angular/cdk/layout';
import { ShrinkHeaderService } from '../../../shared/providers/utils/shrink-header.service';

@Component({
    selector: 'bsc-dashboard-component',
    templateUrl: './bsc-dashboard.component.html',
    styleUrls: ['bsc-dashboard.component.scss'],
    providers: [MediaMatcher]
})
export class BscDashboardComponent {

    @ViewChild('bscsidenav') bscsidenav;
    @ViewChild('more') matCard;
    private _mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
    UNKNOWN_USER_IMAGE: string = SharedConstants.PATHS.UNKNOWN_USER_IMAGE;
    PRINCIPAL_GAUGE: any = SharedConstants.GAUGE.PRINCIPAL_GAUGE;
    FINALCIAL_GAUGE: any = SharedConstants.GAUGE.FINALCIAL_GAUGE;
    CLIENT_GAUGE: any = SharedConstants.GAUGE.CLIENT_GAUGE;
    INTERNAL_PROCCESS_GAUGE: any = SharedConstants.GAUGE.INTERNAL_PROCCESS_GAUGE;

    title: string = "Dashboard";
    displayTitle: boolean = false;
    isMore: boolean;
    values: any = 25;

    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _shrinkHeaderSrv: ShrinkHeaderService,
        private _renderer: Renderer,
        private _media: MediaMatcher
    ) {
        this.mobileQuery = this._media.matchMedia('(max-width: 1024px)');
        this._mobileQueryListener = () => this._changeDetectorRef.detectChanges();

        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnChanges() {
        this._shrinkHeaderSrv.suscriberHeader.emit(this.mobileQuery);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
        // this._shrinkHeaderSrv.suscriber.unsubscribe();
    }

    closeSidenav() {
        this.bscsidenav.close();
    }

    openSidenav() {
        this.bscsidenav.toggle();
    }

    getAvatarClass(feed): string {
        let baseClass = 'cs-avatars ';
        let numberText: string;

        switch (feed.files.length) {
            case 0: return baseClass.trim();
            case 1: numberText = 'one'; break;
            default: numberText = 'more-images'; break;
        }
        return baseClass + numberText;
    }
}