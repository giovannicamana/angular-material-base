
import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'cs-user-avatar',
    templateUrl: './cs-user-avatar.component.html',
    styleUrls: ['cs-user-avatar.component.scss']
})

export class CsUserAvatarComponent {
    @Input() users: any[];
    @Input() usersCount: number;

    @Output() onSelect = new EventEmitter();

    constructor() { }

    goToProfile(userId) {
        //TODO: gotoProfile
        // this.onSelect.emit({
        // 	index,
        // 	_id: userId
        // });
    }

    getEntityClass(): string {
        let baseClass = 'cs-avatars ';
        let numberText: string;
        //-TODO: para demo cambiaremos esta parte
        this.usersCount = document.querySelectorAll('cs-user-avatar .cs-avatar').length;

        if (this.usersCount) {
            switch (this.usersCount) {
                case 0: return baseClass.trim();
                case 1: numberText = 'one'; break;
                // case 2: numberText = 'two'; break;
                // case 3: numberText = 'three'; break;
                // case 4: numberText = 'four'; break;
                default: numberText = 'more-images'; break;
            }
            return baseClass + numberText;
        }

    }
}