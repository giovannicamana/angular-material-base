import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'cs-accordion',
    templateUrl: './cs-accordion.component.html',
    styleUrls: ['cs-accordion.component.scss']
})

export class CsAccordion {
    @Input() title: string;

    @Input() active: boolean = false;

    @Output() toggleAccordion: EventEmitter<boolean> = new EventEmitter();

    constructor() { }

    onClick(event) {
        event.preventDefault();
        this.toggleAccordion.emit(this.active);
    }
}