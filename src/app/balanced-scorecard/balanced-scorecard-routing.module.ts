import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BscWelcomeComponent } from './pages/bsc-welcome/bsc-welcome.component';
import { BscDashboardComponent } from './pages/bsc-dashboard/bsc-dashboard.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: BscWelcomeComponent, pathMatch: 'full' },
        { path: 'dashboard', component: BscDashboardComponent },
    ])],
    exports: [RouterModule]
})
export class BalancedScorecardRoutingModule {}