import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

import { BaseService } from "../../core/base.service";
import { HeadersService } from "../../core/headers.service";
import 'rxjs/add/operator/map';

import { Owner } from "../../shared/models/owner";
import { CommonService } from "../../core/common.service";

let END_POINT = "api/agents"

@Injectable()
export class DemoService extends BaseService<Owner>{
    constructor(
        private _http: HttpClient,
        private _headersSrv: HeadersService,
        private _commonSrv: CommonService
    ) {
        super(END_POINT, _http, _headersSrv, _commonSrv);
    }

    getAgentById(_id: string): Observable<any> {
        let query = this._commonSrv.normalizeQuery(END_POINT + "/get", { _id });

        return this._http.get(query)
            .map((res: any) => {
                if (res.success) return res.result;
                else throw res.error;
            });
    }
}



