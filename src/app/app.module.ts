import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Augmentations } from '../augmentations/Augmentations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CoreModule } from './core/core.module';
import { MainModule } from './main/main.module';
import { SharedModule } from './shared/shared.module';

import { MatToolbarModule, MatSidenavModule, MatIconModule } from '@angular/material';
// import { DemoCompoenentComponent } from './demo-compoenent/demo-compoenent.component';


Augmentations.init();

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule, FormsModule,
        BrowserAnimationsModule,
        MainModule,
        SharedModule,
        CoreModule.forRoot(),
        AppRoutingModule,

        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
    ],
    declarations: [
        AppComponent,
        // DemoCompoenentComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
