import { Injectable } from "@angular/core";

let ENV = 'dev';
let IP = '192.168.0.101';
let PORT = 5200;

@Injectable()

export class ConfigService {
    BASE_URL: string;
    constructor() {
        this.BASE_URL = this._getBaseUrl();
    }

    private _getBaseUrl(): string {
        let urls = {
            local: `http://${IP}:${PORT}/`,
            dev: 'https://api.github.com/',
            production: ''
        };
        return urls[ENV];
    }
}