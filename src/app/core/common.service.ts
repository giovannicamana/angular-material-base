import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {

    constructor(private _configSrv: ConfigService) { }

    buildApiUrl(endPoint: string, ...params: string[]): string {
        if (!endPoint.endsWith('/')) endPoint += '/';

        let apiUrl = this._configSrv.BASE_URL + endPoint;
        return apiUrl + params.join('/');
    }

    normalizeQuery(endPoint: string, criteria: any): string {
        let apiUrl = this.buildApiUrl(endPoint).slice(0, -1);
        return apiUrl + this._getQueryString(criteria);
    }

    private _getQueryString(criteria: any): string {
        let keys = Object.keys(criteria);
        if (!keys.length) return '';
        let query = keys.map(key => `${key}=${criteria[key]}&`).join('');
        return '?' + query.slice(0, -1);
    }

    setBackgrounStyle(bgImage?: string): any {
        //-TODO:CommonImage
        bgImage = bgImage || '';

        return { 'background-image': `url(${bgImage})` };
    }

    validatePhone(phone: any): string {
        if (typeof (phone) === 'string') return phone;

        if (typeof (phone) === 'object') {
            let cell = phone['cell'];

            if (cell) return cell;

            for (let key in phone) {
                if (phone[key]) return phone[key];
            }
        }

        throw 'No phone number provided.';
    }

    contains(text: string, val: string): boolean {
        return text.indexOf(val) > -1;
    }

}
