import { Component, OnInit, Input, OnChanges, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';
import { ShrinkHeaderService } from '../../../shared/providers/utils/shrink-header.service';

@Component({
    selector: 'header-component',
    templateUrl: './header.component.html',
    styleUrls: ['header.component.scss']
})
export class HeaderComponent {

    @Output('onToggleMode') onToggleMode = new EventEmitter();
    @Output('onToggleSide') onToggleSide = new EventEmitter();

    @Input() title: string;
    @Input() displayTitle: boolean;

    hideHeader: boolean;

    constructor(
        private _shrinkingHeaderSrv: ShrinkHeaderService

    ) { }
    ngOnChanges() {
        this._shrinkingHeaderSrv.suscriber
            .subscribe(istrue => {
                this.displayTitle = istrue;
            });

        this._shrinkingHeaderSrv.suscriberHeader
            .subscribe(hideHeader => {
                this.hideHeader = hideHeader;
            });
    }

    toggleMode() {
        this.onToggleMode.emit();
    }

    toggleSide() {
        this.onToggleSide.emit();
    }
}
