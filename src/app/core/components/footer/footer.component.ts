import { Component, Input } from "@angular/core";
import { Owner } from '../../../shared/models/owner';
@Component({
    selector: 'footer-component',
    templateUrl: './footer.component.html',
    styleUrls: ['footer.component.scss']
})
export class FooterComponent {
    @Input() subtitle = '';
    owner: Owner;

    constructor(
    ) {
    }
}

