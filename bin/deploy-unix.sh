# exit script -- just in case
abort() { echo "$@" 1>&2 ; exit 1; }
# kill message when dead
KILL="Invalid Command"

read -p 'Commit description: ' desc
read -p 'Which environment? [production or staging]: ' env

if [ "$desc" = "" ]
then
    abort "Commit description not provided"
elif [ "$env" = "" ]
then
    abort "Environment name not provided"
fi

if [ "$env" = "staging" ] || [ "$env" = "production" ]
then
    git checkout dev && \
    git add . -A && \
    git add -u && \
    git commit -m "$desc" && \
    git checkout master && \
    git merge dev && \
    git push "$env" master && \
    git checkout dev

    echo "Pushing to $env environment was successful"
else
    abort "$KILL"
fi
