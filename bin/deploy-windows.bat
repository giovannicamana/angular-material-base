@echo off

:commit
set /p desc=Commit description:
if "%desc%" == "" (
    echo.
    echo "Commit description not provided"
    goto:commit
)
:environment
set /p env=Which environment? [production or staging]:
if "%env%" == "" (
    echo.
    echo "Environment name not provided"
    echo.
    goto:environment
)

:deploy
set res=F
if "%env%" == "staging" ( set res=T )

if %res% NEQ T (
    if "%env%" == "production" ( set res=T )
)
:calledRoutine
setlocal
%@Try%
  if %res% == T (

    git checkout dev && git add . -A && git add -u && git commit -m "%desc%" && git checkout master && git merge dev && git push "%env%" master && git checkout dev

    echo.
    echo Pushing to %env% environment was successful
    echo.
) else (
    echo.
    echo Invalid environment %env%
)
%@EndTry%
:@Catch
  REM Exception handling code goes here
	echo.
	git checkout dev
:@EndCatch
